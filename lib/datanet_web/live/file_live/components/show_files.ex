defmodule DataNetWeb.Files.ShowFiles do
  use DataNetWeb, :live_component

  @impl true
  def mount(socket) do
    {:ok, assign(socket, name: ""), temporary_assigns: [name: ""]}
  end
end
