defmodule DataNetWeb.FileLive.Index do
  use DataNetWeb, :live_view

  alias DataNet.Files
  alias DataNet.Files.File

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :files, list_files())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit File")
    |> assign(:file, Files.get_file!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New File")
    |> assign(:file, %File{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Files")
    |> assign(:file, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    file = Files.get_file!(id)
    {:ok, _} = Files.delete_file(file)

    {:noreply, assign(socket, :files, list_files())}
  end

  defp list_files do
    Files.list_files()
  end
end
