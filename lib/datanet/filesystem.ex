defmodule DataNet.Filesystem do
  def current_directory() do
    File.cwd()
  end

  def listdir("."), do: listdir("./priv/data")

  def listdir(path) do
    {:ok, files} = File.ls(path)
    files
  end
end
