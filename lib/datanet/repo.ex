defmodule DataNet.Repo do
  use Ecto.Repo,
    otp_app: :datanet,
    adapter: Ecto.Adapters.Postgres
end
