defmodule DataNet.FilesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `DataNet.Files` context.
  """

  @doc """
  Generate a file.
  """
  def file_fixture(attrs \\ %{}) do
    {:ok, file} =
      attrs
      |> Enum.into(%{
        name: "some name"
      })
      |> DataNet.Files.create_file()

    file
  end
end
